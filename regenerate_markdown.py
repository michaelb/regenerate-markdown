#!/usr/bin/env python

import markdown
import os
import sys
import re
import Tkinter

from glob import glob

INCLUDE = re.compile(r'&lt;!--\s*#include\s*"([^"\n]+)"\s*--&gt;')

base_path = os.path.dirname(os.path.abspath(__file__))
def apply_serverside(match):
    path = match.group(1)
    #print path
    #print match

    # abs paths are relative to this file's base path
    if path.startswith('/'):
        path = path.lstrip('/')
    path = os.path.join(base_path, path)

    try:
        return open(path).read()
    except:
        return "<strong>Could not open '%s'!</strong>" % path



def regenerate():
    processed_paths = []
    for root, dir, files in os.walk(base_path):
        for path in files:
            if not path.endswith(".md"):
                continue
            markdown_contents = open(path).read()
            base, ext = os.path.splitext(path)
            new_path = base + ".html"
            html = markdown.markdown(markdown_contents)
            open(new_path, "w+").write(html)
            processed_paths.append(new_path)

    # now apply the includes
    for path in processed_paths:
        html = open(path).read()

        new_html = INCLUDE.sub(apply_serverside, html)
        if new_html != html:
            open(path, "w+").write(new_html)


class simpleapp_tk(Tkinter.Tk):
    def __init__(self,parent):
        Tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()

    def initialize(self):
        self.grid()

        #self.entry = Tkinter.Entry(self)
        #self.entry.grid(column=0,row=0,sticky='EW')

        button = Tkinter.Button(self,text=u"Regenerate!!", font=("Purisa", 24), command=self.on_click)
        button.grid(column=0,row=0)

        self.grid_columnconfigure(0,weight=1)
        self.resizable(False, False)

    def on_click(self):
        regenerate()



def main():
    if len(sys.argv) > 1:
        regenerate()
        # args, assume not gui
    else:
        app = simpleapp_tk(None)
        app.title('Regenerate markdown')
        app.mainloop()


if __name__ == '__main__':
    main()

