
Usage
----

A super simple app I made to help less tech savvy people maintain simple
markdown based static websites! Drop it in a directory where you keep your
markdown files and then run it when you wanna regenerate them.


When run without args it is a single window that has one button to regenerate
markdown in the given directory (e.g. every file that ends with ".md" gets a
".html" that represents it).

SSI Include
----

It also supports basic includes, in SSI style syntax, for example here we
include LICENSE:

    <!--#include "LICENSE" -->

These files are not processed but included verbatim (however, it could be
another generated HTML file, however SSI in that file might not have been yet
applied).

